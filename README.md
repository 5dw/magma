# Magma

Lavachat backend.

## Setting it up

 - Have Elixir installed.

```
git clone https://gitlab.com/lavachat/magma
cd magma

cp config/config.example.exs config/config.exs
# edit config.exs as needed for your server

# run!
iex -S mix
```
