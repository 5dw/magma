defmodule Magma.MixProject do
  use Mix.Project

  def project do
    [
      app: :magma,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Magma, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:msgpax, "~> 2.0"},
      {:postgrex, "~> 0.13.3"},
      {:cowboy, "~> 2.4.0"}
    ]
  end
end
