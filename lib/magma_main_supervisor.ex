defmodule Magma.Supervisor do
  use Supervisor

  require Logger

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    Supervisor.init([
      {Magma.CowboySupervisor, :start}
    ], strategy: :one_for_one)
  end
end
