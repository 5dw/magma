defmodule Magma.IndexHandler do
  def init(req0, state) do
    req = :cowboy_req.reply(200,
      %{"content-type" => "text/plain"},
      "helo", req0
    )

    {:ok, req, state}
  end
end

defmodule Magma.CowboySupervisor do
  use Supervisor

  require Logger

  def build_dispatch do
    :cowboy_router.compile([
      {:_, [
        {"/", Magma.IndexHandler, nil},
      ]}
    ])
  end

  def start_link(opts) do
    Logger.info "starting cowboy"
    dispatch = build_dispatch()

    port = Application.fetch_env!(:magma, :port)

    # TODO: https
    :cowboy.start_clear(
      :magma,
      [port: port],
      %{env: %{dispatch: dispatch}}
    )
  end
end
