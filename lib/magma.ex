defmodule Magma do
  use Application

  require Logger

  def start(_type, _args) do
    Logger.info "Starting Magma"

    Supervisor.start_link(
      [
        {Magma.Supervisor, name: {:global, Magma.Supervisor}}
      ],
      name: {:global, Magma.MainSupervisor},
      strategy: :one_for_one
    )
  end
end
